'use strict';
import express from 'express';
const app = express();

import serverOsm from './osm/router-osm.js';

// Security:
// https://expressjs.com/en/advanced/best-practice-security.html#at-a-minimum-disable-x-powered-by-header
app.disable('x-powered-by');

app.all('*', function (req, res, next) {
  console.log(req.url);
  next();
});

const corsWildcard = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  next();
};

app.use('/osm', corsWildcard, serverOsm);

export default app;
