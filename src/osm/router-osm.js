import express from 'express';
const router = express.Router();
import RouterElement from './element/router-element.js';

router.use('/:type/:id', async function (req, res, next) {
  const { type, id } = req.params;
  if (!['node', 'way', 'relation'].includes(type)) {
    res.status(404).end('Type needs to be node, way or relation');
    return;
  }
  const parsedId = Number.parseInt(id);
  if (Number.isNaN(parsedId)) {
    res.status(404).end('Element id needs to be a valid integer');
    return;
  }
  req.osmType = type;
  req.osmId = parsedId;
  next();
}, RouterElement);

export default router;
