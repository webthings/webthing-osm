import fs from 'fs';

const cache = {
  node: {},
  way: {},
  relation: {},
};

export function loadFromFile() {
  let data = {};
  try {
    data = JSON.parse(fs.readFileSync('./data/cache.json', 'utf8'));
  } catch (error) {
    console.warn('Could not read element cache. Ignorning cache.');
    console.warn(error);
  }
  cache.node = data.node || {};
  cache.way = data.way || {};
  cache.relation = data.relation || {};
}

export function saveToFile() {
  fs.writeFileSync('./data/cache.json', JSON.stringify(cache, null, 2));
}

export function getFromCache(type, id) {
  return cache[type][id];
}

export function saveToCache(type, id, element) {
  cache[type][id] = element;
  saveToFile();
}
