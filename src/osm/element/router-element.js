import express from 'express';
const router = express.Router();

import bundlerMiddleware from './bundler-middleware.js';

import * as Nominatim from '../apis/nominatim.js';
import * as Overpass from '../apis/overpass.js';
const askNominatimById = bundlerMiddleware(Nominatim.getElementById);
const askOverpassById = bundlerMiddleware(Overpass.getElementById);
import { loadFromFile, getFromCache, saveToCache } from './cache.js';

import RouterOpeningHours from './opening-hours/router-opening-hours.js';

loadFromFile();

async function getOsmElement(type, id) {
  let element = getFromCache(type, id);
  if (!element) {
    console.log(`Asking server for ${type}(${id})`);
    const nominatimObject = await askNominatimById(type, id);
    const osmObject = await askOverpassById(type, id);
    if (!nominatimObject) throw Error('No result from Nominatim');
    if (!osmObject) throw Error('No result from Overpass');
    const osmTags = osmObject.tags || {};
    element = {
      id: nominatimObject.osm_id,
      name: osmTags.name,
      opening_hours: osmTags.opening_hours,
      timezone: nominatimObject.timezone,
      nominatimObject,
      osmObject,
    };
    if (element) saveToCache(type, id, element);
  }
  return element;
}

router.use('/', function (req, res, next) {
  getOsmElement(req.osmType, req.osmId).then((osmElement) => {
    req.osmElement = osmElement;
    next();
  }).catch((error) => {
    //res.status(404).end('Element not found in OpenStreetMap');
    res.status(500).end('Failed to retrieve the information from other server');
    console.error(error);
  });
});

router.use('/opening-hours', RouterOpeningHours);

export default router;
