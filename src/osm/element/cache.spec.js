import { saveToCache, getFromCache} from './cache.js';

describe('can store and receive', () => {
  const element = {
    id: 34,
  };
  beforeAll(() => {
    saveToCache('node', element.id, element);
  });

  test('can receive the element', () => {
    expect(getFromCache('node', 34)).toBe(element);
  });
  test('gets undefined on a different ID', () => {
    expect(getFromCache('node', 33)).toBe(undefined);
  });
  test('gets undefined on a different type', () => {
    expect(getFromCache('way', 34)).toBe(undefined);
  });
});
