export default function createBundler(func) {
  const pendingPromises = {
    node: {},
    way: {},
    relation: {},
  };

  return (type, id) => {
    if (pendingPromises[type][id]) {
      return pendingPromises[type][id];
    }
    const promise = func(type, id);
    pendingPromises[type][id] = promise;
    return promise.finally(() => {
      pendingPromises[type][id] = undefined;
    });
  }
}
