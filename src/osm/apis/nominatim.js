const timeLimit = 30 * 1000;

let lastRequest = new Date().getTime() - timeLimit;

export async function askNominatim(elementIds) {
  if (!process.env.NOMINATIM_URL) {
    return Promise.reject('No Nominatim URL specified!');
  }
  if (lastRequest + timeLimit > new Date().getTime()) {
    return Promise.reject('Configured Nominatim exceeded!');
  }
  lastRequest = new Date().getTime();
  const res = await fetch(`${process.env.NOMINATIM_URL}/lookup?format=json&osm_ids=${elementIds.join(',')}`, {
    headers: {
      'Accept': 'application/json',
      'User-Agent': 'https://gitlab.com/webthings/webthing-osm/',
    },
  });
  if (!res.ok) {
    throw Error(`Failed to fetch Nominatim API ${res.status} ${await res.text()}`);
  }
  return res.json();
}

export async function getElementById(type, id) {
  if (!['node', 'way', 'relation'].includes(type)) throw new Error(`Unknown type: ${type}`)
  const elementId = `${type.charAt(0).toUpperCase()}${id}`;
  const result = await askNominatim([elementId]);
  const element = result.find((element) => (
    element.osm_id === id && element.osm_type === type
  ));
  return element;
}
