const timeLimit = 30 * 1000;

let lastRequest = new Date().getTime() - timeLimit;

export async function askOverpass(codestring) {
  if (!process.env.OVERPASS_URL) {
    return Promise.reject('No Overpass URL specified!');
  }
  if (lastRequest + timeLimit > new Date().getTime()) {
    return Promise.reject('Configured Overpass exceeded!');
  }
  lastRequest = new Date().getTime();
  const res = await fetch(`${process.env.OVERPASS_URL}/interpreter`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'User-Agent': 'https://gitlab.com/webthings/webthing-osm/',
    },
    body: codestring,
  });
  if (!res.ok) {
    throw Error(`Failed to fetch Overpass API: ${res.status} ${await res.text()}`);
  }
  return res.json();
}

export async function getElementById(type, id) {
  if (!['node', 'way', 'relation'].includes(type)) throw new Error(`Unknown type: ${type}`)
  const query = `
  [out:json][timeout:25];
  (${type}(${id}););
  out body;>;out skel qt;
  `;
  const result = await askOverpass(query);
  const element = result.elements.find((element) => (
    element.id === id && element.type === type
  ));
  return element;
}
