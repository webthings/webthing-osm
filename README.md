# WebThing: OpenStreetMap
This server exposes Points of interest on OpenStreetMap as WebThings.

## Configuration
Copy the file `.env-sample` to `.env` and configure the server and integrations.

* `PORT` – The TCP port this server will listen to. (default: 3000)
* `NOMINATIM_URL` – A URL to send [Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim) requests to
* `OVERPASS_URL` – A URL to send [Overpass](https://wiki.openstreetmap.org/wiki/Overpass_API) requests to
* `USER_AGENT` – Define a user agent for all requests to external APIs. They commonly expect you to specify a string unique to your project or instance.

## First start
```bash
npm ci     # Installs dependencies
npm start  # Runs the server
```

## Connect a controller
Point the controller to `http://localhost:3000/osm/way/92406904/opening-hours` to get the opening hours of Vancouver Public Library – Renfrew branch.

## Things

### Opening Hours
![](./docs/opening-hours.png)
